class DataService {
  constructor() {
    this.provider = useLocalStorage ? 'ls' : 'idb';
  }

  // Working with REST here

  async sendToServer(data) {
    try {
      await fetch('/api/chart', {
        method: 'post',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data),
      });
    } catch (error) {
      console.error('Cannot fetch data: ', error);
    }
  }

  async getFromServer() {
    try {
      const data = await fetch('/api/chart');
      return data.json();
    } catch (error) {
      console.error('Cannot fetch data: ', error);
    }
  }

}

