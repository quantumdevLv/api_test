/*
$('#calendar').datepicker({
		});

!function ($) {
    $(document).on("click","ul.nav li.parent > a ", function(){          
        $(this).find('em').toggleClass("fa-minus");      
    }); 
    $(".sidebar span.icon").find('em:first').addClass("fa-plus");
}

(window.jQuery);
	$(window).on('resize', function () {
  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
})
$(window).on('resize', function () {
  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
})

$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
	if(!$this.hasClass('panel-collapsed')) {
		$this.parents('.panel').find('.panel-body').slideUp();
		$this.addClass('panel-collapsed');
		$this.find('em').removeClass('fa-toggle-up').addClass('fa-toggle-down');
	} else {
		$this.parents('.panel').find('.panel-body').slideDown();
		$this.removeClass('panel-collapsed');
		$this.find('em').removeClass('fa-toggle-down').addClass('fa-toggle-up');
	}
})

$(document).ready(function () {

    var ctx = document.getElementById("myChart").getContext("2d");

    var data = {
        labels: ['start'],
        datasets: [{
            label: "Temperature",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [0]
        }, {
            label: "Humidity",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [0]
        }]
    };
    var options = {
        animation: false,
        //Boolean - If we want to override with a hard coded scale
        scaleOverride: true,
        //** Required if scaleOverride is true **
        //Number - The number of steps in a hard coded scale
        scaleSteps: 3,
        //Number - The value jump in the hard coded scale
        scaleStepWidth: 10,
        //Number - The scale starting value
        scaleStartValue: 0
    };

    var myLineChart = new Chart(ctx).Line(data, options);

    var socket = io();

    socket.on('newData', function (newData) {
    	console.log(newData);
    	var time = new Date();
    	if (newData.temperature <= 0 || newData.humiditi <= 0)  {
    	    return false;
        }

        data.datasets[0].data.push(newData.temperature);
        data.datasets[1].data.push(newData.humiditi);

		data.labels.push(time.getMinutes() + ':' + time.getSeconds());

		if(data.datasets[0].data.length > 20) {
            data.labels.shift();
            data.datasets[0].data.shift();
            data.datasets[1].data.shift();
		}

        if(newData.temperature > (options.scaleSteps * options.scaleStepWidth)) {
            options.scaleSteps = Math.ceil(newData.temperature/options.scaleStepWidth)
        }

        if(newData.humiditi > (options.scaleSteps * options.scaleStepWidth)) {
            options.scaleSteps = Math.ceil(newData.humiditi/options.scaleStepWidth)
        }

        var myLineChart = new Chart(ctx).Line(data, options);

		return true;
    });

})
*/