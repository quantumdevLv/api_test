const Router = require('express').Router;
const router = Router();
const chartController = require('../controllers/chart.controller');

router.get('/chart', chartController.getChart);
router.post('/chart', chartController.createChart);
router.put('/chart', chartController.updateChart);
router.delete('/chart', chartController.deleteChart);

module.exports = router;