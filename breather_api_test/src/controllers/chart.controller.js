const Chart = require('../model/ChartModel');
var ObjectID = require('mongodb').ObjectID;

const getChart = async (req, res) => {
  try {
    const chart = await Chart.find().lean();
    res.json(charts);
  } catch (error) {
    res
      .status(500)
      .json({ error });
  }
}

const createChart = async (req, res) => {
  try {
    const chart = await Chart.create(req.body);
    res.json(chart);
  } catch (error) {
    res
      .status(500)
      .json({ error });
  }
}

const updateChart = async (req, res) => {
  try {
	const id = req.params.id;
    const chart = await Chart.updateOne({"_id": ObjectID(req.body.id)}, req.body, {new: true});
    res.json(chart).save();
  } catch (error) {
    res
      .status(500)
      .json({ error });
  }
}

const deleteChart = async (req, res) => {
  try {
	const id = req.params.id;
    const chart = await Chart.findOneAndDelete({"_id": ObjectID(req.body.id)});
    
  } catch (error) {
    res
      .status(500)
      .json({ error });
  }
}


module.exports = {
  getChart,
  createChart,
  updateChart,
  deleteChart,
}