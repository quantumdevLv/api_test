
var GrovePi = require('node-grovepi').GrovePi

var mongoose = require('mongoose');
var ChartModel = require('./model/ChartModel');

module.exports = {
    init: function(io) {

        var Commands = GrovePi.commands
        var Board = GrovePi.board

        var DHTDigitalSensor = GrovePi.sensors.DHTDigital

        var board = new Board({
            debug: true,
            onError: function(err) {
                console.log('Something wrong just happened')
                console.log(err)
            },

            onInit: function (res) {
                if (res) {
                    console.log('GrovePi Version :: ' + board.version())

                    var dhtSensor = new DHTDigitalSensor(7, DHTDigitalSensor.VERSION.DHT11, DHTDigitalSensor.CELSIUS)
                    console.log('DHT Digital Sensor (start watch)')
                    dhtSensor.on('change', function (res) {
                        var time = new Date();
                        console.log('DHT onChange value=' + res[1])
                        console.log('Temperature=' + res[0] + ' Humidity=' + res[1] + '%')
                        if (res[0] <= 100 && res[1] <= 100) {
                            io.emit('newData', {
                                temperature: res[0],
                                humidity: res[1],
                                time: time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds()
                            })
                            var ChartData = new ChartModel({
                                _id: mongoose.Types.ObjectId(),
                                temperature: res[0],
                                humidity: res[1],
                                time: time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds(),
                                date: time
                            })

                            ChartData.save().then(data => console.info('data saved : ' + JSON.stringify(data)))
                                .catch(error => console.error(error));
                        }

                    })

                    dhtSensor.watch(500) // milliseconds
                }
            }
        })
        board.init()

    }
}