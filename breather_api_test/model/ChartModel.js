var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ChartSchema = new Schema({
    _id: ObjectId,
    temperature: Number,
    humidity: Number,
    time: String,
    date: Date
});

module.exports = mongoose.model('Chart', ChartSchema);